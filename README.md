<img src="https://capsule-render.vercel.app/api?type=waving&color=timeGradient&height=300&section=header&text=Nuclear%20AI%20Cyber&fontSize=90&animation=scaleIn&desc=Idaho%20State%20University%20CEADS%20efforts%20in%20nuclear%20AI%20and%20cybersecurity&descAlignY=70" />

This effort seeks to identify possible cyber vulnerablities with machine learning and explore how these could be applied to proposed AI applications in nuclear science/engineering. In addition this effort seeks to provide reccomendations and advice to vendors, end-users and policy makers.


## Table of contentents 
- [Projects](#projects)
- [Contributors](#contributors)
- [Resources](#resources)


## Projects
The CEADS team has prototyped three attack surfaces: 
1. [inference attacks](https://gitlab.com/CEADS/nuclear_ai_cyber/-/tree/main/AutoKeras_Inference-Attack) to “steal” or replicate the machine learning model with varying levels of information, 
2. [trojan attacks](https://gitlab.com/CEADS/nuclear_ai_cyber/-/tree/main/trojan) to infiltrate and falsify machine learning systems with “look-a-like” systems that have triggers for certain malicious behavior, and 
3. adversarial reprogramming to minimally alter inputs that then trick machine learning systems into targeted misclassification. We found that autonomous systems are significantly vulnerable to these attacks.
4. [reverse engineering](https://gitlab.com/CEADS/nuclear_ai_cyber/-/tree/main/reverse_engineer)
    * [BlackBox](https://gitlab.com/CEADS/nuclear_ai_cyber/-/tree/main/reverse_engineer/BlackBox)
    * [GreyBox](https://gitlab.com/CEADS/nuclear_ai_cyber/-/tree/main/reverse_engineer/GreyBox)
    * [WhiteBox](https://gitlab.com/CEADS/nuclear_ai_cyber/-/tree/main/reverse_engineer/WhiteBox)

## Contributors
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Resources 
1. Introduction
    - [CEADS Website](https://ceads.gitlab.io/members/)
    - [Summer 2022 Report](https://www.osti.gov/biblio/1901802)
2. Python & Machine Learning
    - [Python Website](https://www.python.org/)
    - [PyCharm IDE](https://www.jetbrains.com/pycharm/)
    - [Atom Text Editor](https://github.blog/2022-06-08-sunsetting-atom/)
    - [Anaconda Python Distribution](https://www.anaconda.com/download)
    - [Scikit Learn Website](https://scikit-learn.org/stable/)
    - [TPOT AutoML](http://epistasislab.github.io/tpot/)
    - [H2O Machine Learning/AutoML](https://docs.h2o.ai/h2o/latest-stable/h2o-docs/welcome.html)
    - [Keras Website](https://keras.io/)
    - [PyTorch Website](https://pytorch.org/)
    - [Handbook of Anomaly Detection with Python Outlier Detection -- (12) Autoencoders](https://towardsdatascience.com/anomaly-detection-with-autoencoder-b4cdce4866a6)
3. Web Articles
    - [Handbook of Anomaly Detection with Python Outlier Detection -- (12) Autoencoders](https://towardsdatascience.com/anomaly-detection-with-autoencoder-b4cdce4866a6)
4. Cyber Security
    - [Cyber Security Full Course In 8 Hours](https://www.youtube.com/watch?v=nzZkKoREEGo)
    - [247 CTF](https://247ctf.com/)
    - [Cyber Warrior CTF](https://www.cyberwarrior.com/ctf/)
    - [How the Best Hackers Learn Their Craft](https://www.youtube.com/watch?v=6vj96QetfTg)
    - [Network Engineering Video](https://www.youtube.com/watch?v=qiQR5rTSshw)
    - [The Internet: Crash Course Computer Science (Videos 28-34)](https://www.youtube.com/playlist?list=PL8dPuuaLjXtNlUrzyH5r6jN9ulIgZBpdo)
    - [Introduction to Networking — Network Fundamentals Part 1](https://www.youtube.com/watch?v=cNwEVYkx2Kk)
    - [What is a Subnet Mask?](https://avinetworks.com/glossary/subnet-mask/)
    - [TryHackMe — Cyber Security Training](https://tryhackme.com/hacktivities)
5. Machine Learning Cyber Security
    - [How to attack Machine Learning ( Evasion, Poisoning, Inference, Tro- jans, Backdoors)](https://towardsdatascience.com/how-to-attack-machine-learning-evasion-poisoning-inference-trojans-backdoors-a7cb5832595c)
    - [What Every Machine Learning Company Can Learn from the Zillow- pocalypse](https://medium.com/@shanwhiz/what-every-machine-learning-company-can-learn-from-the-zillow-pocalypse-8d473e8348d0)
    - [Adversarial Machine Learning 101](https://github.com/mitre/advmlthreatmatrix/blob/master/pages/adversarial-ml-101.md)
    - [A survey of adversarial machine learning in cyber warfare](https://www.researchgate.net/publication/327074374_A_Survey_of_Adversarial_Machine_Learning_in_Cyber_Warfare)
    - [Evasion attacks on Machine Learning (or “Adversarial Examples”)](https://towardsdatascience.com/evasion-attacks-on-machine-learning-or-adversarial-examples-12f2283e06a1)
    - [TrojanNN Github](https://github.com/PurduePAML/TrojanNN)
    - [Neural Trojan Attacks and How You Can Help Article](https://towardsdatascience.com/neural-trojan-attacks-and-how-you-can-help-df56c8a3fcdc)
    - [Learn about nuclear energy, https://whatisnuclear.com](https://whatisnuclear.com/)
    - [Nuclear Power Plant, https://www.nuclear-power.com/nuclear-power- plant/](https://www.nuclear-power.com/nuclear-power-plant/)
    - [Intro to Reactor Physics, https://www.nuclear-power.com/nuclear-power/reactor- physics/](https://www.nuclear-power.com/nuclear-power/reactor-physics/)
    - [InstrumentationandControlSystemsforNuclearPowerPlants,IAEA.org](https://www.iaea.org/topics/operation-and-maintenance/instrumentation-and-control-systems-for-nuclear-power-plants#:~:text=I%26C%20%E2%80%93%20a%20Nuclear%20Power%20Plant's%20Central%20Nervous%20System&text=Through%20its%20constituent%20elements%2C%20such,the%20NPP's%20operations%20as%20necessary)
    - [Digital Instrumentation and Control, https://www.nrc.gov/reactors/digital.html](https://www.nrc.gov/reactors/digital.html)
    - [Stuxnet explained: The first known cyberweapon](https://www.csoonline.com/article/3218104/stuxnet-explained-the-first-known-cyberweapon.html)
